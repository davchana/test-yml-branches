[![build status](https://gitlab.com/davchana/test-yml-branches/badges/master/build.svg)](https://gitlab.com/davchana/test-yml-branches/commits/master)

## Purpose:

This project is to test .yml with different branches.
_(how to use gitlab pages with different branches at same time, instead of merging/pushing every commit of branches bar or foo to master)_

## OK, Results:

_(Last Updated 2016-10-31)_

* latest build result url: [https://davchana.gitlab.io/test-yml-branches/](https://davchana.gitlab.io/test-yml-branches/)
* latest build result url-beta: [https://davch-beta.gitlab.io/test-yml-branches/](https://davchana.gitlab.io/test-yml-branches/)

#### Project Structure

* _project_ foo : _htps://username.gitlab.io/foo_ (Main Project, Live, In Production)
* _project_ foo : _htps://groupname.gitlab.io/foo_ (Fork from Main, for testing, merge back to main once results are stable)

#### File/Repo Structure:

_push all changes in Main to Fork to keep them in sync/same-folder-structure_

* _branch_ master
    * _file_ .gitlab-ci.yml
    * _folder_ public
        * _file_ index.html
* _branch_ 1-issue
    * _file_ .gitlab-ci.yml
    * _folder_ public
        * _file_ index.html
        
#### Common Code in all .gitlab-ci.yml files (of all branches):

````yml
    image: alpine:latest
    pages:
    stage: deploy
    script:
        - echo 'Nothing to do...'
    artifacts:
    paths:
        - public
    only:
````

#### Difference in each .gitlab-ci.yml files (in each branches):
_appended below above code._

````diff
    -master
````
    
where `master` is branch name
a `-` is appended to branch name.

#### Flow:

1. A commit is made in anywhere in branch `foo`, it triggers a run & then a build (coded in .yml file)
3. Once page is deployed (run is successfull), `index.html` of __"that"__ branch (foo) is available at https://_username_.gitlab.io/_project-name_
4. Now, when  acommit is made in any other branch `bar`, it again triggers a run & then a build (coded in .yml file)
5. Once it is successful, now __"this"__ index.html of branch (bar) is available at that same url.
 

#### Notes:

* Use all testing on davch-beta branches, & it will be available on _davch-beta.gitlab.io/project-name_
* If all good, merge it to davchana branches, & it will be available on _davchana.gitlab.io/project-name_
* davch-beta & davchana will NOT share the same data in local Storage (different domains).

So, make branches, keep them in sync with each other as explained at [Git Branching Model](http://nvie.com/posts/a-successful-git-branching-model/), any last commit/edit to any branch will deploy that version on your pages, anytime bad thing happens, either revert; or make a commit in last good known branch.
